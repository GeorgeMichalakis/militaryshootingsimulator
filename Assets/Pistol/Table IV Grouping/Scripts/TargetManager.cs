﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer[] targets = new MeshRenderer[5];
    [SerializeField]
    private MeshCollider[] targetColliders = new MeshCollider[5];
    private MeshCollider currentTargetCollider;
    private int roundsPerTarget = 0;
    private int numOfTarget = 0;
    [SerializeField]
    private bool targetsEnabled = true;
    void Start()
    {
        for (int i = 1; i < 5; i++)
            targets[i].enabled = targetsEnabled;

        for (int i = 1; i < 5; i++)
            targetColliders[i].enabled = targetsEnabled;
        currentTargetCollider = targetColliders[0];
    }
    public void MouseHit(Collider hitCollider)
    {
        if (!targetsEnabled)
        {
            if (hitCollider == currentTargetCollider)
            {
                UpdateTargets();
            }
        }
    }

    private void UpdateTargets()
    {
        if (roundsPerTarget < 4)
        {
            roundsPerTarget++;
            if (roundsPerTarget == 4 && numOfTarget<4)
            {
                numOfTarget++;
                StartCoroutine(WaitBeforeRender(0.5f,numOfTarget));
                currentTargetCollider = targetColliders[numOfTarget];
                roundsPerTarget = 0;
            }
        }
    }
    IEnumerator WaitBeforeRender(float seconds, int numOfTarget)
    {
        yield return new WaitForSeconds(seconds);
        targets[numOfTarget].enabled = true;
        targetColliders[numOfTarget].enabled = true;
    }
}
