﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private AudioSource gunShotAudioManager;
    private LayerMask targetMask = 1 << 8;
    [SerializeField]
    private int maxNumOfShots = 0;
    private int totalBulletShots = 0;
    void Update()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray,out hit))
        {
            transform.position = hit.point;
            if (Input.GetMouseButtonDown(0))
            {
                Fire();
                if (Physics.Raycast(ray, out hit, 109f, targetMask))
                {
                    hit.collider.gameObject.transform.parent.GetComponent<TargetManager>().MouseHit(hit.collider);
                }
            }
        }
        if (totalBulletShots >= maxNumOfShots)
        {
            transform.gameObject.SetActive(false);
        }
    }

    private void Fire()
    {
        totalBulletShots++;
        gunShotAudioManager.Play();
    }
}
